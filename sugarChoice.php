<?php

if ($_SERVER["REQUEST_METHOD"] !== "POST") {
    header("Location: drinkChoice.php");
}

if (!isset($_POST["drink"])) {

    header("Location: drinkChoice.php");
}

require("data.php");
$drinksFiltered = array_filter($drinks, function ($drink) {
    return strtolower($drink["name"]) === strtolower($_POST["drink"]);
});

$drinkSelected = array_values($drinksFiltered)[0];

// Si il n'y a pas de resultat
if (!isset($drinkSelected)) {

    header("Location: drinkChoice.php?error=" . $_POST["drink"]);
}

// Si la boisson n'accepte pas le sucre
if (!$drinkSelected["sugar"]) {

    header("Location: orderValidation.php?drink=" . $drinkSelected["name"] . "&sugar=0&price=" . $drinkSelected["price"]);
}

?>



<!DOCTYPE html>
<html lang="fr">

<head>
    <meta charset="UTF-8">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <title>Choix sucre</title>
</head>

<body>
    <h2>Choix sucre</h2>
    <form action="orderValidation.php" method="POST">

        <label for="sugar">La dose de sucre</label>
        <input id="sugar" type="number" min="0" max="5" name="sugar">

        <input type="hidden" name="drink" value="<?php echo ($drinkSelected["name"]); ?>">

        <input type="hidden" name="price" value="<?php echo ($drinkSelected["price"]); ?>">

        <input type="submit" value="Valider">

    </form>
</body>

</html>