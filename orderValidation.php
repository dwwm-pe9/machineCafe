<?php

if ($_SERVER["REQUEST_METHOD"] !== "POST") {
    header("Location: drinkChoice.php");
}

if (!isset($_POST["drink"],$_POST["price"],$_POST["sugar"]) ) {

    header("Location: drinkChoice.php");
}

?>
<!DOCTYPE html>
<html lang="fr">

<head>
    <meta charset="UTF-8">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <title>Validation commande</title>
</head>

<body>
    <h2>Votre commande: <?php echo ($_POST["drink"] . " avec " . $_POST["sugar"] . " sucre(s), total : " . $_POST["price"] . " €") ?></h2>
    <a>Commander ( paiement )</a>
    <a href="index.php">Annuler ma commande</a>
</body>

</html>