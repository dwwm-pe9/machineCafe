<!DOCTYPE html>
<html lang="fr">

<head>
    <meta charset="UTF-8">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <title>Choix boisson</title>
</head>

<body>
    <h2>Choix boisson</h2>
    <div>
        <?php
        require("data.php");
        foreach ($drinks as $drink) {
            echo ("<p>" . $drink["name"] . "</p>");
        }
        ?>
    </div>
    <?php
    if (isset($_GET["error"])) {
        echo ("<p class='text-danger'>" . $_GET["error"]  . " n'est pas dans la liste des boissons</p>");
    }
    ?>
    <form action="sugarChoice.php" method="POST">
        <label for="drink">Votre boisson</label>
        <input type="text" name="drink" id="drink" required>
        <input type="submit" value="Valider">
    </form>
</body>

</html>