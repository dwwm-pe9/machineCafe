<?php

$drinks = [

    [
        "name" => "café",
        "price" => 1.4,
        "sugar" => true,
        "milk" => true,
    ],
    [
        "name" => "Chocolat chaud",
        "price" => 0.4,
        "sugar" => true,
        "milk" => false,
    ],
    [
        "name" => "Eau",
        "price" => 1,
        "sugar" => false,
        "milk" => false,
    ],

];

?>